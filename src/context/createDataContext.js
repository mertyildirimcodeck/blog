// This is template file for coming other Contexts..

import React from "react";

export default (reducer, actions, initialState) => {
  const Context = React.createContext(undefined, undefined);

  const Provider = ({ children }) => {
    const [state, dispatch] = React.useReducer(reducer, initialState);

    //actions = {addBlogPost: (dispatch)=>{return ()=>{}}
    const boundActions = {};
    for (let key in actions) {
      boundActions[key] = actions[key](dispatch);
    }
    return (
      <Context.Provider value={{ state, ...boundActions }}>
        {children}
      </Context.Provider>
    );
  };

  return { Context, Provider };
};
