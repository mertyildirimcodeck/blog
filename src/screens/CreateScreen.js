import React from "react";
import { Context as BlogContext } from "../context/BlogContext";
import BlogForm from "../components/BlogForm";

const CreateScreen = ({ navigation }) => {
  const { addBlogPost } = React.useContext(BlogContext);

  return (
    <BlogForm
      onSubmit={(title, content) =>
        addBlogPost(title, content, () => {
          navigation.navigate("Index");
        })
      }
    />
  );
};

export default CreateScreen;
