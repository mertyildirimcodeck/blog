import React from "react";
import { Context as BlogContext } from "../context/BlogContext";
import BlogForm from "../components/BlogForm";

const EditScreen = ({ navigation }) => {
  const id = navigation.getParam("id");
  const { state, editBlogPost } = React.useContext(BlogContext);
  const blogPost = state.find((blogPost) => {
    return blogPost.id === id && blogPost.id;
  });
  return (
    <BlogForm
      initialValues={{
        title: blogPost.title,
        content: blogPost.content,
      }}
      onSubmit={(title, content) =>
        editBlogPost(id, title, content, () => navigation.pop())
      }
    />
  );
};

export default EditScreen;
